package br.com.bianeck.springboot.web.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/params")
public class ExemploParamsController {

    @GetMapping({"/", ""})
    public String index() {
        return "params/index";
    }


    @GetMapping("/string")
    public String param(@RequestParam(name = "texto", required = false, defaultValue = "Algum valor") String texto, Model model) {
        model.addAttribute("resultado", "O parãmetro enviado é: " + texto);
        return "params/ver";
    }

    @GetMapping("/mix-params")
    public String param(@RequestParam String saudacao, @RequestParam Integer numero, Model model) {
        model.addAttribute("resultado", "O parametro saudacao enviado é: '" + saudacao + "' e o número é '" + numero + "'.");
        return "params/ver";
    }

    @GetMapping("/mix-params-request")
    public String param(HttpServletRequest request, Model model) {
        String saudacao = request.getParameter("saudacao");
        Integer numero = null;
        try {
            numero = Integer.parseInt(request.getParameter("numero"));
        } catch(NumberFormatException e) {
            numero = 0;
        }
        model.addAttribute("resultado", "O parametro saudacao enviado é: '" + saudacao + "' e o número é '" + numero + "'.");
        return "params/ver";
    }
}
