package br.com.bianeck.springboot.web.app.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/variaveis")
public class ExemploVariaveisRotaController {

    @GetMapping("/string/{texto}")
    public String variaveis(@PathVariable String texto, Model model) {
        model.addAttribute("titulo", "Receber parâmetros da rota (@PathVariable)");
        model.addAttribute("resultado", "O texto enviado na rota é: " + texto);
        return "variaveis/ver";
    }
}
