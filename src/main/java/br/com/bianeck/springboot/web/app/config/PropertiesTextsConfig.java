package br.com.bianeck.springboot.web.app.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;

@Configuration
@PropertySources({
        @PropertySource(factory = YamlPropertySourceFactory.class, value = "classpath:texts.yaml")
})
public class PropertiesTextsConfig {
}
