package br.com.bianeck.springboot.web.app.controllers;

import br.com.bianeck.springboot.web.app.models.Usuario;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Arrays;
import java.util.List;

@Controller
@RequestMapping("/app")
public class IndexController {

    @Value("${text.indexcontroller.index.title}")
    private String indexText;
    @Value("${text.indexcontroller.profile.title}")
    private String profileText;
    @Value("${text.indexcontroller.list.title}")
    private String listText;

    @GetMapping({"/index", "/", "", "/home"})
    public String index(Model model) {
        model.addAttribute("titulo", indexText);
        return "index";
    }

    @GetMapping("/profile")
    public String profile(Model model) {
        Usuario usuario = new Usuario();
        usuario.setNome("Thiago");
        usuario.setSobrenome("Bianeck");
//        usuario.setEmail("thiagobianeck@gmail.com");
        model.addAttribute("titulo", profileText.concat(usuario.getNome()));
        model.addAttribute("usuario", usuario);
        return "perfil";
    }

    @GetMapping("/list")
    public String list(Model model) {
        model.addAttribute("titulo", listText);
        return "listar";
    }

    @ModelAttribute("usuarios")
    public List<Usuario> fillUsers() {
        return Arrays.asList(
                new Usuario("Thiago", "Bianeck", "thiagobianeck@gmail.com"),
                new Usuario("Roger", "Logan", "rogerlogan@gmail.com"),
                new Usuario("Clara", "Isis", "claraisis@gmail.com"),
                new Usuario("Lunna", "Maitê", "lunnamaite@gmail.com")
        );
    }
}
